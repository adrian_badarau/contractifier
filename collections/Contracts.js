/**
 * Created by adrianbadarau on 10/04/16.
 */

class ContractsModel extends BaseModel {
    getSchema(){
        return new SimpleSchema({
            name:{
                type:String,
                label:'Name',
                max:255
            },
            category:{
                type:String,
                label:'Contract Category',
                autoform:{
                    afFieldInput:{
                        options(){return ContractCategories.getForSelect('name','_id')},
                        firstOption:'Select Category...'
                    }
                }
            },
            createdAt:{
                type:Date,
                autoValue(){
                    if(this.isInsert){
                        return new Date();
                    }else if(this.isUpsert){
                        return { $setOnInsert: new Date() }
                    }else {
                        this.unset()
                    }
                }
            },
            updatedAt:{
                type:Date,
                autoValue(){
                    return new Date();
                }
            },
            owner:{
                type:String
            },
            fields:{
                type:Array
            },
            'fields.$':{
                type:Object
            },
            'fields.$.name':{
                type:String,
                label:'Field Name'
            },
            'fields.$.value':{
                type:String,
                label:'Field value',
                optional:true,
                blackbox:true
            }
        });
    }
}
Contracts = new ContractsModel('Contracts');
Contracts.allow({
    insert: function (userId, doc) {
        // the user must be logged in, and the document must be owned by the user
        return (userId && doc.owner === userId);
    },
    update: function (userId, doc, fields, modifier) {
        // can only change your own documents
        return doc.owner === userId;
    },
    remove: function (userId, doc) {
        // can only remove your own documents
        return doc.owner === userId;
    }
});
Contracts.attachSchema(Contracts.getSchema());
