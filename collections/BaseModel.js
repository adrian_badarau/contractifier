/**
 * Created by adrianbadarau on 11/04/16.
 */
BaseModel = class BaseModel extends Mongo.Collection {
    getForSelect(label,value){
        let userId = Meteor.userId();
        if(userId){
            let items = this.find({owner:Meteor.userId()}).fetch();
            let list = [];
            for(let item of items){
                list.push({label:item[label],value:item[value]});
            }
            return list;
        }else{
            throw new Meteor.Error('NotLoggedIn','Please log in to use this method');
        }
    }
    getSchema(){
        return new SimpleSchema({})
    }
};