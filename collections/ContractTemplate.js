/**
 * Created by adrianbadarau on 10/04/16.
 */
class ContractTemplateModel extends BaseModel {
    getSchema() {
        return new SimpleSchema({
            name: {
                type: String,
                label: 'Name',
                max: 255
            },
            fields: {
                type: Array,
                label: 'Search fields'
            },
            'fields.$': {
                type: Object
            },
            'fields.$.key': {
                type: String,
                label: 'Search field binding'
            },
            'fields.$.value': {
                type: String,
                label: 'Search field value'
            },
            owner: {
                type: String
            },
            createdAt: {
                type: Date,
                autoValue(){
                    if (this.isInsert) {
                        return new Date();
                    } else if (this.isUpsert) {
                        return {$setOnInsert: new Date()}
                    } else {
                        this.unset()
                    }
                }
            },
            updatedAt: {
                type: Date,
                autoValue(){
                    return new Date();
                }
            }
        })
    }
}
ContractTemplates = new ContractTemplateModel('ContractTemplates');
ContractTemplates.allow({
    insert: function (userId, doc) {
        // the user must be logged in, and the document must be owned by the user
        return (userId && doc.owner === userId);
    },
    update: function (userId, doc, fields, modifier) {
        // can only change your own documents
        return doc.owner === userId;
    },
    remove: function (userId, doc) {
        // can only remove your own documents
        return doc.owner === userId;
    }
});
ContractTemplates.attachSchema(ContractTemplates.getSchema());

