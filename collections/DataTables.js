/**
 * Created by adrianbadarau on 16/04/16.
 */
TabularTables = {};
TabularTables.ContractsTable = new Tabular.Table({
    name:"ContractsTable",
    collection: Contracts,
    columns:[
        {data:'name', title:'Contract Name'},
        {data:'owner', title:'Creator'}
    ]
});
