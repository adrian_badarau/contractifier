/**
 * Created by adrianbadarau on 11/04/16.
 */
class ContractCategoriesModel extends BaseModel{
    getSchema(){
        return new SimpleSchema({
            name:{
                type:String,
                label:"Category name",
                max:55
            },
            owner:{
                type:String
            },
            createdAt:{
                type:Date,
                autoValue(){
                    if(this.isInsert){
                        return new Date();
                    }else if(this.isUpsert){
                        return { $setOnInsert: new Date() }
                    }else {
                        this.unset()
                    }
                }
            },
            updatedAt:{
                type:Date,
                autoValue(){
                    return new Date();
                }
            }
        });
    }
}
ContractCategories = new ContractCategoriesModel('ContractCategories');
ContractCategories.allow({
    insert: function (userId, doc) {
        // the user must be logged in, and the document must be owned by the user
        return (userId && doc.owner === userId);
    },
    update: function (userId, doc, fields, modifier) {
        // can only change your own documents
        return doc.owner === userId;
    },
    remove: function (userId, doc) {
        // can only remove your own documents
        return doc.owner === userId;
    }
});
ContractCategories.attachSchema(ContractCategories.getSchema());