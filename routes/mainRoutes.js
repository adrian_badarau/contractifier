/**
 * Created by adrianbadarau on 07/04/16.
 */
FlowRouter.route('/dashboard',{
    name:'Dashboard',
    action(){
        BlazeLayout.render('Main', {content:'Dashboard'})
    }
});

FlowRouter.route('/settings',{
    name:'UserSettings',
    action(){
        BlazeLayout.render('Main',{content:'UserSettings'})
    }
});
