/**
 * Created by adrianbadarau on 10/04/16.
 */

let contractRoutes = FlowRouter.group({
    prefix:'/contracts',
    name:'contracts',
});

contractRoutes.route('/index',{
    name:'contractsIndex',
    action(){
        BlazeLayout.render('Main',{content:'ContractsIndex'})
    }
});
contractRoutes.route('/manage/:_id?',{
    name:'contractsManage',
    action(){
        BlazeLayout.render('Main', {content: 'ContractsManage'})
    }
});