/**
 * Created by adrianbadarau on 16/04/16.
 */
let ContractTemplatesController = FlowRouter.group({
    prefix:'/contract-templates',
    name:'ContractTemplates'
});

ContractTemplatesController.route('/manage/:_id?',{
    name:'ContractTemplatesManage',
    action(){
        BlazeLayout.render('Main',{content:'ContractTemplatesManage'})
    }
});