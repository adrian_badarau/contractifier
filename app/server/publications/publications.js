/**
 * Created by adrianbadarau on 11/04/16.
 */
import {Meteor} from 'meteor/meteor';
Meteor.publish('Contracts',function () {
    return Contracts.find({owner:this.userId});
});
Meteor.publish('ContractCategories',()=>{
    return ContractCategories.find({});
});
Meteor.publish('SingleContract', function (contractId) {
    return Contracts.find({_id:contractId, owner:this.userId});
});
Meteor.publish('SingleContractTemplate', function (templateId) {
    return ContractTemplates.find({owner:this.userId, _id:templateId});
});