/**
 * Created by adrianbadarau on 11/04/16.
 */
export let CustomHooks = {
    addOwner:{
        before:{
            insert(doc){
                if(Meteor.userId()){
                    doc.owner = Meteor.userId();
                    return doc;
                }
            }
        }
    }
};