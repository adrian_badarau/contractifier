/**
 * Created by adrianbadarau on 11/04/16.
 */

class ContractsIndex extends BlazeComponent {
    constructor() {
        super();
        console.log('From constructor');
        this.contracts = Contracts.find({});
    }

    onCreated() {
        console.log("GES", this);
    }

    onRendered() {

    }

    events() {
        return [{
            'click #addNewContract'(event){
                FlowRouter.go('contractsManage');
            },
            'click tbody > tr' (event) {
                let dataTable = $(event.target).closest('table').DataTable();
                let rowData = dataTable.row(event.currentTarget).data();
                FlowRouter.go('contractsManage',{_id:rowData._id});
                console.log(rowData);
                if (!rowData) return; // Won't be data if a placeholder row is clicked
                // Your click handler logic here
            }
        }];
    }

    getDataTable(){
    }
}
ContractsIndex.register('ContractsIndex');
