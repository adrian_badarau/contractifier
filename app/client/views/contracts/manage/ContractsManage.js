/**
 * Created by adrianbadarau on 11/04/16.
 */
import {Meteor} from 'meteor/meteor';
import {CustomHooks} from "/app/client/AutoFormHooks";

class ContractsManage extends BlazeComponent {
    constructor() {
        super();
        
    }

    onCreated() {
        this.autorun(()=>{
            this.subscribe('ContractCategories');
            if(FlowRouter.getParam('_id')){
                console.log('dasdas',FlowRouter.getParam('_id'));
                this.subscribe('SingleContract',FlowRouter.getParam('_id'))
            }
        })
    }

    onRendered() {
        console.log("SdsadaDASD");
    }

    events() {
        return [{
            'click #addContractCategory'(event){
                event.preventDefault();
            }
        }];
    }

    getContract(){
        return Contracts.findOne({_id:FlowRouter.getParam('_id')});
    }
    getFormType(){
        if(FlowRouter.getParam('_id')){
            return 'update'
        }
        return 'insert'
    }
}
ContractsManage.register('ContractsManage');

AutoForm.addHooks('manageContracts',CustomHooks.addOwner);