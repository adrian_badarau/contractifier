/**
 * Created by adrianbadarau on 16/04/16.
 */
import {Meteor} from 'meteor/meteor';

class ContractTemplatesManage extends BlazeComponent {
    constructor() {
        super();
    }

    template() {
        return 'ContractTemplatesManage';
    }

    onCreated() {
        this.autorun(()=>{
            if(FlowRouter.getParam('_id')){
                this.subscribe('SingleContractTemplate',FlowRouter.getParam('_id'))
            }
        })
    }

    onRendered() {

    }

    events() {
        return [{}];
    }

    getFormType(){
        if(FlowRouter.getParam('_id')){
            return 'update'
        }
        return 'insert'
    }

    getDoc(){
        return ContractTemplates.findOne({_id:FlowRouter.getParam('_id')})
    }
}
ContractTemplatesManage.register('ContractTemplatesManage');
let contractTemplateFormHook = {
    before:{
        update(doc){
            console.log("from Hook:" ,doc);

            return (doc);
        },
        insert(doc){
            if(Meteor.userId()) {
                doc.owner = Meteor.userId();
            }
            console.log('from insert', doc);
            return (doc);
        }
    }
};
AutoForm.addHooks("manageContractTemplatesForm",contractTemplateFormHook, true);
