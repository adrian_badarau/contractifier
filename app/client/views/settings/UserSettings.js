/**
 * Created by adrianbadarau on 16/04/16.
 */

class UserSettings extends BlazeComponent {
    constructor() {
        super();
    }

    template() {
        return 'UserSettings';
    }

    onCreated() {

    }

    onRendered() {

    }

    events() {
        return [{
            'submit #dropBoxForm'(event){
                event.preventDefault();
                console.log('dasdas the form', event);
                let dropBoxSettings = {
                    key:event.target.dropBoxKey,
                    secret:event.target.dropBoxSecret,
                    token:event.target.dropBoxToken
                }
            }
        }];
    }
}
UserSettings.register('UserSettings');