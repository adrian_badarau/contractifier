/**
 * Created by adrianbadarau on 11/04/16.
 */
import {CustomHooks} from "../../AutoFormHooks"
class CreateContractCategoryModal extends BlazeComponent {
    constructor() {
        super();
    }

    onCreated() {

    }

    onRendered() {

    }

    events() {
        return [{
            'submit #createContractCategoryForm'(event){
                $('#createContractCategoryModal').modal('toggle');
            }
        }];
    }
}
CreateContractCategoryModal.register('CreateContractCategoryModal');

AutoForm.addHooks('createContractCategoryForm',CustomHooks.addOwner);