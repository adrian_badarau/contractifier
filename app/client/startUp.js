/**
 * Created by adrianbadarau on 11/04/16.
 */
import {Meteor} from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base';

Meteor.startup(()=>{
    Accounts.ui.config({
        passwordSignupFields:"USERNAME_AND_EMAIL"
    })
});