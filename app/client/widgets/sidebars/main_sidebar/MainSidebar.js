class MainSidebar extends BlazeComponent {
    constructor() {
        super();
    }

    onCreated() {

    }

    onRendered() {

    }

    events() {
        return [{
            'click #addNewContractCategory'(event){
                event.preventDefault(); 
            }
        }];
    }
}
MainSidebar.register('MainSidebar');